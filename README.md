# Installer Prometheus pour monitorer le cluster Minikube/K8s sur Docker Desktop

Pour comprendre les stratégies de déploiement et mise à jour d’applications dans Kubernetes (deployment and rollout strategies), nous allons installer puis mettre à jour une application d’exemple et observer comment sont gérées les requêtes vers notre application en fonction de la stratégie de déploiement choisie.

Pour cette observation, nous allons utiliser un outil de monitoring. Nous utiliserons ce TP comme prétexte pour installer une des stacks les plus populaires et intégrée avec Kubernetes : **Prometheus** et **Grafana**. Prometheus est un projet de la Cloud Native Computing Foundation.

Prometheus est un serveur de métriques, c’est-à-dire qu’il enregistre des informations précises (de petite taille) sur différents aspects d’un système informatique, et ce de façon périodique, en interrogeant les composants du système (metrics scraping).

**Note :** Pour exécuter l’ensemble des commandes Bash ci-dessous, utilisez l’invite de commande `Git Bash` (disponible avec Git pour Windows) ou un terminal Bash sur votre système.

## Installer Prometheus avec Helm

1. Installez Helm si ce n’est pas déjà fait. Sur Ubuntu :
   ```bash
   sudo snap install helm --classic
   ```

2. Créez un namespace pour Prometheus et Grafana :
   ```bash
   kubectl create namespace monitoring
   ```

3. Ajoutez le dépôt de charts **Prometheus** et **kube-state-metrics** :
   ```bash
   helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
   helm repo add kube-state-metrics https://kubernetes.github.io/kube-state-metrics
   helm repo update
   ```

4. Installez ensuite le chart Prometheus :
   ```bash
   helm install \
     --namespace=monitoring \
     --version=26.0.0 \
     --set=service.type=NodePort \
     prometheus \
     prometheus-community/prometheus
   ```

## kube-state-metrics et le monitoring du cluster

Le chart officiel installe par défaut, en plus de Prometheus, kube-state-metrics, qui est une intégration automatique de Kubernetes et Prometheus.

Une fois le chart installé, vous pouvez visualiser les informations dans Lens, dans la première section du menu de gauche `Cluster`.

## Déployer notre application d’exemple (goprom) et la connecter à Prometheus

Nous allons installer une petite application d’exemple en langage Go.

1. Téléchargez le code de l’application et de son déploiement depuis GitLab :
   ```bash
   git clone https://gitlab.com/devops_tps/k8s-deployment-strategies.git
   ```

2. Appliquez ce déploiement Kubernetes :
   ```bash
   cd 'k8s-deployment-strategies/k8s_stategies/1 - recreate'
   kubectl apply -f app-v1.yaml
   ```

## Observons notre application et son déploiement Kubernetes

- Explorez le fichier de code Go de l’application `main.go` ainsi que le fichier de déploiement `app-v1.yml`.

### Routes exposées par l’application

- L’application est accessible sur le port `8080` du conteneur et la route `/`.
- L’application expose en plus deux routes de diagnostic (`probe`) Kubernetes sur le port `8086` :
  - `/live` pour la liveness
  - `/ready` pour la readiness
- `goprom` expose une route dédiée au monitoring Prometheus sur le port `9101` : `/metrics`

### Configurer Prometheus pour récupérer les métriques

Si le service `prometheus-server` est en mode `ClusterIP`, passez-le en mode `NodePort` via Lens ou avec un `kubectl edit` sur le service.  
Vous pourrez ensuite accéder à Prometheus via `http://localhost:<nodePort>`.

Dans Prometheus, vérifiez que les métriques de l’application sont récupérées via la requête PromQL :
  ```promql
  sum(rate(http_requests_total{app="goprom"}[5m])) by (version)
  ```

### Annotations pour Prometheus

Dans le `Deployment` de l’application, les annotations indiquant à Prometheus où récupérer les métriques sont :
```yaml
annotations:
  prometheus.io/scrape: "true"
  prometheus.io/port: "9101"
```

## Installer et configurer Grafana

Grafana est une interface de dashboard de monitoring facilement intégrable avec Prometheus. Elle va nous permettre d’afficher un histogramme en temps réel du nombre de requêtes vers l’application.

1. Créez un secret Kubernetes pour stocker le login admin de Grafana :
   ```bash
   cat <<EOF | kubectl apply -n monitoring -f -
   apiVersion: v1
   kind: Secret
   metadata:
     namespace: monitoring
     name: grafana-auth
   type: Opaque
   data:
     admin-user: $(echo -n "admin" | base64 -w0)
     admin-password: $(echo -n "admin" | base64 -w0)
   EOF
   ```

2. Installez Grafana :
   ```bash
   helm repo add grafana https://grafana.github.io/helm-charts
   helm repo update
   helm install \
     --namespace=monitoring \
     --version=8.6.4 \
     --set=admin.existingSecret=grafana-auth \
     --set=service.type=NodePort \
     --set=service.nodePort=32001 \
     grafana \
     grafana/grafana
   ```

3. Accédez à Grafana via `http://localhost:32001`  
   Identifiants par défaut : `admin/admin`.

### Ajouter une DataSource Prometheus dans Grafana

- Dans Grafana, ajoutez une datasource :
  - Name: prometheus
  - Type: Prometheus
  - URL: http://prometheus-server
  - Access: Server (default)

### Créer un Dashboard

- Créez un dashboard avec un panel de type Graph.
- Utilisez la requête Prometheus suivante :
  ```promql
  sum(rate(http_requests_total{app="goprom"}[5m])) by (version)
  ```
- Dans le champ `Legend`, ajoutez `{{version}}` pour distinguer les versions de l’application.

---

## Importer et utiliser le Dashboard Kubernetes Dashboard

Pour aller plus loin dans le monitoring, vous pouvez importer un dashboard complet afin de visualiser des métriques détaillées sur les nœuds du cluster. Le dashboard **Kubernetes Dashboard** (ID 18283) est un exemple très populaire.

### Étapes d’importation d'un dashboard existant

1. **Télécharger le JSON du Dashboard :**  
   Accédez au lien [Kubernetes Dashboard - Dashboard ID 18283](https://grafana.com/grafana/dashboards/18283-kubernetes-dashboard/) et cliquez sur "Download JSON" pour télécharger le fichier sur votre machine.

2. **Importer le Dashboard dans Grafana :**
   - Connectez-vous à Grafana.
   - Dans la barre latérale, cliquez sur le bouton “+” puis sur “Import”.
   - Cliquez sur “Upload JSON file” et sélectionnez le fichier que vous venez de télécharger.
   - Sélectionnez la datasource “prometheus” lors de l’importation.
   - Cliquez sur “Import” pour finaliser.

3. **Visualisation :**
   Le dashboard “Kubernetes Dashboard” est maintenant disponible et vous permettra de visualiser :
   - L’utilisation du CPU, de la mémoire, du disque
   - Les IO système
   - Le réseau
   - Autres métriques de performances et d’état de santé de vos nœuds Kubernetes

Vérifiez dans Prometheus (via `kubectl port-forward -n monitoring service/prometheus-server 9090:80` puis `http://localhost:9090`) que les cibles “node-exporter” sont bien up. Une fois cela confirmé, le dashboard devrait afficher les métriques détaillées.

---